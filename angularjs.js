var app = angular.module('myApp', ['ngCookies']);

app.controller('booksCtrl', function ($scope, $cookies, $window) {
  



    $scope.books = [
        { index: 0, image: "images/book1.jpg", author: 'Christopher Fowler', title: 'Full Dark House', description: 'Full Dark House', price: 95, qty: 1, },
        { index: 1, image: "images/book2.jpg", author: 'Christopher Fowler', title: 'Ten-Second Staircase', description: 'Ten-Second Staircase', price: 19, qty: 1, },
        { index: 2, image: "images/book3.jpg", author: 'Christopher Fowler', title: 'The Hound Of Baskervilles', description: 'The Hound Of Baskervilles', price: 50, qty: 1,  },
        { index: 3, image: "images/book4.jpg", author: 'George Brophy', title: 'The Last Enemy', description: 'The Last Enemy', price: 40, qty: 1},
        { index: 4, image: "images/book5.jpg", author: 'Sheilagh Hodgins', title: 'Mental Disorder and Crime', description: 'Mental Disorder and Crime', price: 60, qty: 1,  },
        { index: 5, image: "images/book6.jpg", author: 'Bessel Ban Der Kolk', title: 'The Body Keep The Score', description: 'The Body Keep The Score', price: 59, qty: 1,   },
        { index: 6, image: "images/book7.jpg", author: 'David Epstein', title: 'Range', description: 'Range', price: 100, qty: 1, },
        { index: 7, image: "images/book8.jpg", author: 'Daniel J Siegerl', title: 'The Whole-Brain Child', description: 'The Whole-Brain Child', price: 70, qty: 1, },
    ];
    var index = 7;
    $scope.cart = [];
    $scope.count = 0;
    $scope.addQuantity = Number(0);
    $scope.newBookAuthor = "";
    $scope.newBookTitle = "";
    $scope.newDescription = "";
    $scope.newBookPrice = Number(0);
    $scope.files = []; 

    var discounts = function() {       
        $scope.books.forEach(function(item){    
            if (item.price < 20 ) {
                item['discount'] = 7;             
            } 
             if (item.price >= 20 &&
                item.price < 60) {
                item['discount'] = 13;                   
            } 
            if (item.price >= 60 &&
                item.price < 90 ) {
                item['discount'] = 19;
                } 
            if (item .price >= 90 &&
                item .price < 200) {
                item['discount'] = 23;               
            }   
            if (item .price >= 200) {
                item['discount'] = 37;   
            }   
            item['newPrice'] = item.price - (item.price*item.discount)/100;        
          });         
        }
    discounts ();
     
    var findItemByIndex = function (items, index) {
        return _.find(items, function (item) {
            return item.index === index;
        });
    };   

    $scope.getCost = function (item) {
        return item.qty * item.newPrice;
    };
    $scope.buyBook = function (itemToAdd) {
    
        $scope.count = Number($scope.count) + Number(itemToAdd.qty);
        var found = findItemByIndex($scope.cart, itemToAdd.index);
        if (found) {
            found.qty += itemToAdd.qty;
        } else {
            $scope.cart.push(angular.copy(itemToAdd));
        }
        $cookies.cart = JSON.stringify($scope.cart);
        $cookies.count = $scope.count;
        console.log($scope.cart)
    };

    $scope.addBook = function () {
        
        index++;
        if ($scope.newBookAuthor === "" ||
            $scope.newBookTitle === "" ||
            $scope.newDescription ==="" ||
            $scope.newBookPrice === Number(0) ||
            $scope.addQuantity === Number(0) ||
            $scope.files[0] instanceof Object == false) {
            $scope.alert = "Please complete all the fields"
        } else {
            // debugger
            // function exBook(theBook) { 
            //     return theBook.title === $scope.newBookTitle ;
            // }
            // if ($scope.books.find(exBook)) {
            //     $scope.addQuantity++

            // var result;
            // result = $scope.books.filter(function(book) {
            //     return book.author ===  $scope.newBookTitle 
            // })

            // for (var i= 0; i < $scope.books.length; i++) {
            //     var book = $scope.books[i];

            //     if (book.author ===  $scope.newBookTitle) {
            //         result.push(book);
            //     }
            // }

            // if (result.length === 1) {
            //     for (var j = 0; j < $scope.books.length; j++) {
            //         if ($scope.books[j].index === result.index) {
            //             $scope.books[j].qty += result.qty;
            //         }

            //     }
            // }

            $scope.alert = ""
            $scope.books.push({
                index: Number(index),
                image: $scope.files[0].url,
                author: $scope.newBookAuthor,
                title: $scope.newBookTitle,
                description:  $scope.newDescription,
                price: $scope.newBookPrice,
                qty: $scope.addQuantity
            });     
          
        }  
        discounts (); 
    }
    $scope.removeBook = function () {
        $scope.books.pop();
    }
    $scope.resetBTN = function () {

            $scope.books = [
                { index: 0, image: "images/book1.jpg", author: 'Christopher Fowler', title: 'Full Dark House', description: 'Full Dark House', price: 95, qty: 1 },
                { index: 1, image: "images/book2.jpg", author: 'Christopher Fowler', title: 'Ten-Second Staircase', description: 'Ten-Second Staircase', price: 19, qty: 1 },
                { index: 2, image: "images/book3.jpg", author: 'Christopher Fowler', title: 'The Hound Of Baskervilles', description: 'The Hound Of Baskervilles', price: 50, qty: 1 },
                { index: 3, image: "images/book4.jpg", author: 'George Brophy', title: 'The Last Enemy', description: 'The Last Enemy', price: 40, qty: 1, },
                { index: 4, image: "images/book5.jpg", author: 'Sheilagh Hodgins', title: 'Mental Disorder and Crime', description: 'Mental Disorder and Crime', price: 60, qty: 1 },
                { index: 5, image: "images/book6.jpg", author: 'Bessel Ban Der Kolk', title: 'The Body Keep The Score', description: 'The Body Keep The Score', price: 59, qty: 1 },
                { index: 6, image: "images/book7.jpg", author: 'David Epstein', title: 'Range', description: 'Range', price: 100, qty: 1},
                { index: 7, image: "images/book8.jpg", author: 'Daniel J Siegerl', title: 'The Whole-Brain Child', description: 'The Whole-Brain Child', price: 70, qty: 1 },
            ];
        $scope.addQuantity = Number(0);
        $scope.newBookAuthor = "";
        $scope.newBookTitle = "";
        $scope.newDescription = "";
        $scope.newBookPrice = Number(0);
        $scope.count = 0;
        $scope.cart = [];
        index = 7;
        $scope.alert = "";
        $scope.files = [];
        delete $cookies['count'];
        delete $cookies.cart;
        discounts ();
    }

    $scope.checkout = function () {
        $scope.cart = [];
        $scope.count = 0;
        delete $cookies['count'];
        delete $cookies.cart;
    }
    $scope.removeItem = function (b) {
        var index = $scope.books.indexOf(b);
        $scope.books.splice(index, 1);
    }
    $scope.removeCartItem = function (item) {

        var index = $scope.cart.indexOf(item);
        $scope.cart.splice(index, 1);
        $scope.count = $scope.count - Number(item.qty);

        $cookies.cart = JSON.stringify($scope.cart);
        $cookies.count = $scope.count;

    }
    $scope.getTotal = function () {
        var total = _.reduce($scope.cart, function (sum, item) {
            return sum + $scope.getCost(item);
        }, 0);
        return total;
    };
    $scope.increment = function (item) {
        $scope.count++;
        item.qty++;
        $cookies.cart = JSON.stringify($scope.cart);
        $cookies.count = $scope.count;

    }
    $scope.decrement = function (item) {
        if ($scope.count > 0 && item.qty > 0) {
            $scope.count--;
            item.qty--
            $cookies.cart = JSON.stringify($scope.cart);
            $cookies.count = $scope.count;
        }
    }
    var cookies = function () {
        if ($cookies.cart != null ||
            $cookies.count != null
        ) {
            $scope.cart = JSON.parse($cookies.cart);
            $scope.count = Number($cookies.count);
        }
    }
    cookies();

    $scope.description = function (item) {
        $scope.descr = item.description;
        $('body').addClass('block');
        $('.modal-backdrop').css('display', 'block');
        $('.book-descrip').fadeIn(400);
   
    }
    $scope.close = function () {
        $('body').removeClass('block');
        $('.modal-backdrop').css('display', 'none');
        $('.book-descrip').fadeOut(400);
       
    }   
    console.log($scope.books)
    $(window).scroll(function(){
        var scrollTop = angular.element($window).scrollTop();
        if (scrollTop >= $('#header').height()) {
          $('#header').addClass('sticky');
        } else {
          $('#header').removeClass('sticky');
        }
        console.log(scrollTop);
        console.log($('#header').height())
    });

});
app.directive('ngFileModel', ['$parse', function ($parse) {
    return {

        link: function (scope, element, attrs) {
            var model = $parse(attrs.ngFileModel);
            var isMultiple = attrs.multiple;
            var modelSetter = model.assign;
            element.bind('change', function () {
                var values = [];
                angular.forEach(element[0].files, function (item) {
                    var value = {
                        // File Name 
                        name: item.name,
                        //File Size 
                        size: item.size,
                        //File URL to view 
                        url: URL.createObjectURL(item),
                        // File Input Value 
                        _file: item
                    };
                    values.push(value);
                });
                scope.$apply(function () {
                    if (isMultiple) {
                        modelSetter(scope, values);
                    } else {
                        modelSetter(scope, values[0]);
                    }
                });
            });
        }
    };
}]);

